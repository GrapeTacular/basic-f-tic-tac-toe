﻿open System;

[<EntryPoint>]
let main argv =
    
    // game state
    let mutable human = TicTacToeHelpers.Square.X
    let mutable computer = TicTacToeHelpers.Square.O
    let mutable first = TicTacToeHelpers.Square.X
    let mutable depth = 2

    let mutable playing = true
    let mutable board = TicTacToeHelpers.getEmptyBoard ()

    // try to parse arguments from command line
    try
        let h = argv.[0]
        let f = argv.[1]
        let d = argv.[2] |> int

        if h = "O" then
            human <- TicTacToeHelpers.Square.O
            computer <- TicTacToeHelpers.Square.X
        else
            ()

        if f = "O" then
            first <- TicTacToeHelpers.Square.O
        else
            ()

        if d <> 2 then
            depth <- d
        else
            ()
    with
    | :? Exception as ex ->
        printfn "%s" (ex.ToString ())
        ()

    // helper functions
    let getRowAndColFromChoice (choice : string) : (int * int) =
        let sections = choice.Split [| ' ' |]
    
        if Seq.length sections = 2 then
            try
                let r = sections.[0] |> int
                let c = sections.[1] |> int
                (r, c)
            with
            | :? Exception as ex ->
                (-1, -1)
        else
            (-1, -1)
    
    let computerChoice =
        TicTacToeHelpers.makeComputerChoice computer first

    let printBoard () : unit =
        Console.Clear ()
        printfn "\n%s\n\n" (TicTacToeHelpers.boardToString board)

    let handleComputerChoice () : unit = 
        let (moved, nextBoard) = computerChoice board depth 
        if moved then
            board <- nextBoard
        else
            ()

    let handleHumanChoice () : unit =
        printfn "Type in a row, followed by a column. Example: 1 1\n"
        
        let humanChoice = Console.ReadLine ()
        let (r, c) = getRowAndColFromChoice humanChoice
        
        if TicTacToeHelpers.isAvailablePosition board r c then
            board <- TicTacToeHelpers.getMutatedBoard board human r c
        else
            ()

    let handleBoardInfo () : unit =
        let info = TicTacToeHelpers.getBoardInfo board
        
        if info = TicTacToeHelpers.State.WonX then
            printfn "X has won!\n"
            playing <- false
        else if info = TicTacToeHelpers.State.WonO then
            printfn "O has won!\n"
            playing <- false
        else if info = TicTacToeHelpers.State.Drawn then
            printfn "The game has been a drawn.\n"
            playing <- false

    // main loop
    while playing do
        if first = human then
            printBoard ()
            handleHumanChoice ()
            printBoard ()
            handleBoardInfo ()

            if playing then
                handleComputerChoice ()
                printBoard ()
                handleBoardInfo ()
            else
                ()
        else
            handleComputerChoice ()
            printBoard ()
            handleBoardInfo ()

            if playing then
                printBoard ()
                handleHumanChoice ()
                printBoard ()
                handleBoardInfo ()
            else
                ()

    0 // return an integer exit code
