﻿module Minimax

    open System

    let makeMinimax<'n>
        (isTerminal : 'n -> bool)
        (evaluateNode : 'n -> int)
        (getNodeChildren : 'n -> List<'n>)
        : ('n -> int -> int -> int -> bool -> int) =

        let getMax (a : int) (b : int) : int =
            if a > b then a else b

        let getMin (a : int) (b : int) : int =
            if a < b then a else b

        let rec minimax
            (node : 'n)
            (depth : int)
            (alpha : int)
            (beta : int)
            (maximizer : bool)
            : int =

            if depth < 1 || isTerminal node then
                evaluateNode node
            else
                let children = getNodeChildren node

                let mutable s = 0
                let mutable a = alpha
                let mutable b = beta

                let maximizeFunc (child : 'n) : unit =
                    if b > a then
                        s <- getMax s (minimax child (depth - 1) a b false)
                        a <- getMax a s
                    else
                        ()

                let minimizeFunc (child : 'n) : unit =
                    if b > a then
                        s <- getMin s (minimax child (depth - 1) a b true)
                        b <- getMin b s
                    else
                        ()
            
                if maximizer then
                    s <- Int32.MinValue
                    List.iter maximizeFunc children
                else
                    s <- Int32.MaxValue
                    List.iter minimizeFunc children

                s

        minimax
