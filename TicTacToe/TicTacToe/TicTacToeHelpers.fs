﻿module TicTacToeHelpers

    open System;


    // random generator

    let random = Random ()


    // types

    type State = WonX | WonO | Drawn | InProgress

    type Square = X | O | Empty


    // game logic
    
    let getEmptyBoard () : List<(int * Square)> =
        [for i in 0 .. 8 -> (i, Square.Empty)]

    let isAvailablePosition (board : List<(int * Square)>) (r : int) (c : int) : bool =
        let index = 3 * r + c
        List.length (List.filter (fun (i, s) -> i = index && s = Square.Empty) board) > 0

    let getMutatedBoard (board : List<(int * Square)>) (square : Square) (r : int) (c : int) : List<(int * Square)> =
        let index = 3 * r + c
        List.map (fun (i, s) -> if i = index then (i, square) else (i, s)) board
    
    let getSquares (board : List<(int * Square)>) : List<Square> =
        List.map (fun (_, s) -> s) board
    
    let getRows (board : List<(int * Square)>) : List<List<Square>> =
        [
            List.filter (fun (i, _) -> i < 3) board |> getSquares;
            List.filter (fun (i, _) -> i >= 3 && i < 6) board |> getSquares;
            List.filter (fun (i, _) -> i >= 6 && i < 9) board |> getSquares;
        ]
    
    let getCols (board : List<(int * Square)>) : List<List<Square>> =
        [
            List.filter (fun (i, _) -> i % 3 = 0) board |> getSquares;
            List.filter (fun (i, _) -> (i - 1) % 3 = 0) board |> getSquares;
            List.filter (fun (i, _) -> (i - 2) % 3 = 0) board |> getSquares;
        ]
    
    let getDias (board : List<(int * Square)>) : List<List<Square>> =
        [
            List.filter (fun (i, _) -> i = 0 || i = 4 || i = 8) board |> getSquares;
            List.filter (fun (i, _) -> i = 2 || i = 4 || i = 6) board |> getSquares;
        ]
    
    let seqEval (section : List<Square>) : (int * int * int) =
        let counter (x : int, o : int, e : int) (square : Square) : (int * int * int) =
            if square = Square.X then
                (x + 1, o, e)
            else if square = Square.O then
                (x, o + 1, e)
            else
                (x, o, e + 1)
    
        List.fold counter (0, 0, 0) section
    
    let getEvals (board : List<(int * Square)>) : List<(int * int * int)> =
        let rows = getRows board
        let cols = getCols board
        let dias = getDias board
        
        let rowEvals = List.map seqEval rows
        let colEvals = List.map seqEval cols
        let diaEvals = List.map seqEval dias
    
        List.concat [ rowEvals; colEvals; diaEvals; ]
    
    let boardEval (board : List<(int * Square)>) : (int * int) =
        let scorer (x : int, o : int, e : int) : (int * int) =
            let r = random.Next (-1, 2)
            match (x, o, e) with
            | (3, 0, 0) -> (1000 + r, 0)
            | (0, 3, 0) -> (0, 1000 + r)
            | (2, 0, 1) -> (100 + r, 0)
            | (0, 2, 1) -> (0, 100 + r)
            | _ -> (0, 0)
    
        let scores = getEvals board |> List.map scorer
    
        List.fold (fun (x, o) (xi, oi) -> (x + xi, o + oi)) (0, 0) scores
    
    let isFull (board : List<(int * Square)>) : bool =
        List.forall (fun (_, s) -> s = Square.X || s = Square.O) board
    
    let getWinner (board : List<(int * Square)>) : Square =
        let isTriple (x : int, o : int, e : int) : bool =
            x = 3 || o = 3
    
        let triples = getEvals board |> List.filter isTriple
    
        match triples with
        | (3, _, _) :: tail -> Square.X
        | (_, 3, _) :: tail -> Square.O
        | _ -> Square.Empty
    
    let getBoardInfo (board : List<(int * Square)>) : State =
        match ((isFull board), (getWinner board)) with
        | (true, Square.Empty) -> State.Drawn
        | (_, Square.X) -> State.WonX
        | (_, Square.O) -> State.WonO
        | (_, _) -> State.InProgress
    
    
    // strings
    
    let squareToString (square : Square) : string =
        match square with
        | X -> "X"
        | O -> "O"
        | _ -> "_"
    
    let boardToString (board : List<(int * Square)>) : string =
        getRows board
        |> List.map (fun row -> List.map squareToString row |> String.concat "  ")
        |> String.concat (Environment.NewLine + Environment.NewLine)
    
    
    // minimax helpers
    
    let isTerminalBoard (board : List<(int * Square)>) : bool =
        (getBoardInfo board) <> State.InProgress
    
    let makeBoardEvalScore (target : Square) : (List<(int * Square)> -> int) =
        let boardEvalScore (board : List<(int * Square)>) : int =
            let (xScore, oScore) = boardEval board
            if target = Square.X then
                xScore - oScore
            else
                oScore - xScore
    
        boardEvalScore
    
    let makeBoardChildren (first : Square) (second : Square) : (List<(int * Square)> -> List<List<(int * Square)>>) =
        let getCounts (board : List<(int * Square)>) : (int * int) =
            let getCount (s : Square) : int =
                List.filter (fun (_, si) -> si = s) board |> List.length
            
            (getCount Square.X, getCount Square.O)
    
        let getNextSquare (board : List<(int * Square)>) : Square =
            let (x, o) = getCounts board
            if x = o then first else second
    
        let getBoardChild (board : List<(int * Square)>) (i : int) (square : Square) : List<(int * Square)> =
            getMutatedBoard board square (i / 3) (i % 3)
    
        let boardChildren (board : List<(int * Square)>) : List<List<(int * Square)>> =
            let square = getNextSquare board
            let folder (children : List<List<(int * Square)>>) (i : int, s : Square) : List<List<(int * Square)>> =
                if s = Square.Empty then
                    List.append children [getBoardChild board i square]
                else 
                    children
    
            List.fold folder [] board
    
        boardChildren
    
    let makeComputerChoice
        (computer : Square)
        (first : Square)
        : (List<(int * Square)> -> int -> (bool * List<(int * Square)>)) =

        let max = Int32.MaxValue
        let min = Int32.MinValue

        let second = if first = Square.X then Square.O else Square.X

        let evalScore = makeBoardEvalScore computer
        let boardChildren = makeBoardChildren first second
        let minimax = Minimax.makeMinimax isTerminalBoard evalScore boardChildren

        let computerChoice (board : List<(int * Square)>) (depth : int) : (bool * List<(int * Square)>) =
            let folder
                (s : int, c : List<(int * Square)>)
                (ci : List<(int * Square)>)
                : (int * List<(int * Square)>) =
        
                let si = minimax ci depth min max false

                if si > s then
                    (si, ci)
                else
                    (s, c)

            if (isTerminalBoard board) then
                (false, [])
            else
                let (_, choice) = List.fold folder (min, []) (boardChildren board)
                (true, choice)

        computerChoice
